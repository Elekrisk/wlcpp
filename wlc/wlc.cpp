// wlc.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

#include "lexer.h"
#include "exit.h"


void print_usage()
{
	std::cout << "Usage: wlc <path>\n";
}

int main(int argc, char *argv[])
{
	switch (argc)
	{
		case 0:
			std::cerr << "No arguments supplied when at least one was expected.\nThis should never happen.\n";
			eexit(-1);
		case 2:
			break;
		default:
			print_usage();
			eexit(1);
	}

	std::string path = argv[1];

	std::vector<Token> tokens = lex(path);

	for (Token &t : tokens)
	{
		std::cout << t << std::endl;
	}

	eexit(0);
}

