#pragma once

#include <variant>
#include <vector>
#include <string>

enum class ValueType
{
	String,
	Integer,
	Float
};

enum class NodeType
{
	Command,
	Value
};

struct AST
{
	Node root;
};

struct UserCommand
{
	std::string name;
	Node definition;
};

struct Node
{

};

struct Command : public Node
{
	std::string name;
	std::vector<Node*> arguments;
	Node *parent;
};

struct Value : public Node
{
	ValueType type;
	std::variant<std::string, int, float> string, integer, floating_point;
};