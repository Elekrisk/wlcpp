#pragma once

#include <variant>
#include <vector>

enum class TokenType
{
	Command,
	String,
	Integer,
	Float,
	NONE
};

struct Location
{
	size_t line = 1;
	size_t col = 0;
};

struct Token
{
	TokenType type = TokenType::NONE;
	std::variant<std::string, int, float> string, integer, floating_point;
	size_t indent = 0;
	Location loc;
};

std::ostream &operator<<(std::ostream &os, const Token &t);

std::vector<Token> lex(const std::string &path);