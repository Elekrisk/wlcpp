#include "stdafx.h"

#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "lexer.h"
#include "exit.h"

class CharList
{
	std::string string;
	size_t index = 0;
	bool has_read = false;

public:
	CharList(const std::string &string)
	{
		this->string = string;
	}

	char next()
	{
		if (!has_read)
		{
			has_read = true;
		}
		else
		{
			++index;
		}
		return string[index];
	}
};

std::ostream & operator<<(std::ostream & os, const Token & t)
{
	switch (t.type)
	{
	case TokenType::Command:
		os << "Command '" << std::get<std::string>(t.string) << "' (at " << t.loc.line << ", " << t.loc.col << ") with indent " << t.indent;
		break;
	case TokenType::String:
		os << "String \"" << std::get<std::string>(t.string) << "\" (at " << t.loc.line << ", " << t.loc.col << ") with indent " << t.indent;
		break;
	case TokenType::Integer:
		os << "Integer " << std::get<int>(t.integer) << " (at " << t.loc.line << ", " << t.loc.col << ") with indent " << t.indent;
		break;
	case TokenType::Float:
		os << "Float " << std::get<float>(t.floating_point) << " (at " << t.loc.line << ", " << t.loc.col << ") with indent " << t.indent;
		break;
	default:
		os << "???? (at " << t.loc.line << ", " << t.loc.col << ")";
		break;
	}
	return os;
}

std::vector<Token> lex(const std::string &path)
{
	std::ifstream file;
	file.open(path, std::ios::in | std::ios::binary);
	if (!file.is_open())
	{
		std::cerr << "Unable to open path " << path << ", aborting." << std::endl;
		eexit(1);
	}

	std::vector<Token> tokens;

	FILE *f = fopen(path.c_str(), "rb");

	Location loc;
	char c;
	char last_c;
	Token t;
	std::string value_buffer;
	while (file.good())
	{
		c = fgetc(f);
		if (feof(f))
		{
			if (t.type == TokenType::NONE)
			{
				break;
			}
			else if (t.type == TokenType::String)
			{
				std::cerr << "Unexpected end of file before closed string (after " << loc.line << ", " << loc.col << ")\n";
				eexit(6);
			}
			else
			{
				c = '\n';
			}
		}
		++loc.col;
		//std::cout << c;
		switch (t.type)
		{
		case TokenType::NONE:
			switch (c)
			{
			case '\t':
				++t.indent;
				break;
			case '\n':
				loc.col = 0;
				++loc.line;
				break;
			case '\r':
				break;
			default:
				if (isdigit(c) || c == '-')
				{
					t.type = TokenType::Integer;
					t.loc = loc;
					value_buffer += c;
				}
				if (c == '"')
				{
					t.type = TokenType::String;
					t.loc = loc;
				}
				else
				{
					t.type = TokenType::Command;
					t.loc = loc;
					value_buffer += c;
				}
				break;
			}
			break;
		case TokenType::Integer:
			switch (c)
			{
			case '\n':
				loc.col = 0;
				++loc.line;
				try
				{
					t.integer = std::stoi(value_buffer, nullptr, 10);
				}
				catch (std::invalid_argument &e)
				{
					std::cerr << "Unable to parse integer value " << value_buffer << " (at " << t.loc.line << ", " << t.loc.col << ") because of the following error:\n";
					std::cerr << e.what() << std::endl;
					eexit(5);
				}
				tokens.push_back(t);
				t = Token();
				value_buffer = "";
				break;
			case '\r':
				break;
			default:
				if (isdigit(c))
				{
					value_buffer += c;
				}
				else if (c == '.')
				{
					t.type = TokenType::Float;
					value_buffer += c;
				}
				else
				{
					std::cerr << "Unexpected character '" << c << "' (at " << loc.line << ", " << loc.col << ")\n";
					eexit(2);
				}
				break;
			}
			break;
		case TokenType::Float:
			switch (c)
			{
			case '\n':
				loc.col = 0;
				++loc.line;
				try
				{
					t.floating_point = std::stof(value_buffer, nullptr);
				}
				catch (std::invalid_argument &e)
				{
					std::cerr << "Unable to parse float value " << value_buffer << " (at " << t.loc.line << ", " << t.loc.col << ") because of the following error:\n";
					std::cerr << e.what() << std::endl;
					eexit(5);
				}
				tokens.push_back(t);
				t = Token();
				value_buffer = "";
				break;
			case '\r':
				break;
			default:
				if (isdigit(c) || c == '.')
				{
					value_buffer += c;
				}
				else
				{
					std::cerr << "Unexpected character '" << c << "' (at " << loc.line << ", " << loc.col << ")\n";
					eexit(2);
				}
				break;
			}
			break;
		case TokenType::Command:
			switch (c)
			{
			case '\n':
				loc.col = 0;
				++loc.line;
				t.string = value_buffer;
				tokens.push_back(t);
				t = Token();
				value_buffer = "";
				break;
			case '\r':
				break;
			default:
				value_buffer += c;
				break;
			}
			break;
		case TokenType::String:
			switch (c)
			{
			case '\"':
				t.string = value_buffer;
				tokens.push_back(t);
				t = Token();
				value_buffer = "";
			aaa:
				c = fgetc(f);
				if (c != '\n' && !feof(f))
				{
					if (c == '\r')
					{
						goto aaa;
					}
					std::cerr << "Expected newline or end of file after string (at " << loc.line << ", " << loc.col << ")\n";
					eexit(3);
				}
				break;
			case '\n':
				loc.col = 0;
				++loc.line;
				break;
			case '\r':
				break;
			case '\\':
				c = fgetc(f);
				if (feof(f))
				{
					c = '\\';
				}
				switch (c)
				{
				case 'n':
					value_buffer += '\n';
					break;
				case 'r':
					value_buffer += '\r';
					break;
				case 't':
					value_buffer += '\t';
					break;
				case '"':
					value_buffer += '"';
					break;
				default:
					std::cerr << "Unknown escape code \\" << c << " (at " << loc.line << ", " << loc.col << ")\n";
					eexit(4);
					break;
				}
				break;
			default:
				value_buffer += c;
				break;
			}
			break;
		default:
			std::cerr << "Token type is not yet implemented.\n";
			exit(-2);
		}
		last_c = c;
	}

	return tokens;
}